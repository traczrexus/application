#!/usr/bin/env python

import os
import rospy
import rospkg
import serial
import glob
import threading
import time
import struct

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget, QDialog

from sensors_msg_pb2 import sensor_msg
from motor_msg_pb2 import motor_msg
from valves_msg_pb2 import valves_msg

##### GUI objects cheatsheet #####

### Desired position and velocity
# desiredPositionEdit
# desiredPositionSlider
# maxVelocityEdit

### Sensors data
# currentPositionLabel
# velocityLabel
# pressureDifferenceLabel
# currentLabel
# batteryVoltageLabel

### Valves manipulation
# valve1OnButton
# valve1OffButton
# valve1ResetButton
# valve1TimeEdit
# valve2OnButton
# valve2OffButton
# valve2ResetButton
# valve2TimeEdit

### Stop
# stopButton


class TraczPlugin(Plugin):

    def __init__(self, context):
        super(TraczPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('TraczPlugin')

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_prototype_app'), 'ui', 'tracz_app.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('TraczPluginUi')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

        ## signals and slots connection
        # Communication port configuration
        self._widget.comOpenButton.clicked.connect(self.com_open_connection)
        self._widget.comCloseButton.clicked.connect(self.com_close_connection)
        self._widget.comSettingsButton.clicked.connect(self.com_settings)

        # Valves
        self._widget.valve1OnButton.clicked.connect(lambda: self.com_send_valves(1,
                                                                                 float(self._widget.valve1TimeEdit.text()),
                                                                                 0,
                                                                                 float(self._widget.valve2TimeEdit.text()))
                                                    )
        self._widget.valve1OffButton.clicked.connect(lambda: self.com_send_valves(0,
                                                                                  float(self._widget.valve1TimeEdit.text()),
                                                                                  0,
                                                                                  float(self._widget.valve2TimeEdit.text()))
                                                     )
        self._widget.valve2OnButton.clicked.connect(lambda: self.com_send_valves(0,
                                                                                 float(self._widget.valve1TimeEdit.text()),
                                                                                 1,
                                                                                 float(self._widget.valve2TimeEdit.text()))
                                                    )
        self._widget.valve2OffButton.clicked.connect(lambda: self.com_send_valves(0,
                                                                                  float(self._widget.valve1TimeEdit.text()),
                                                                                  0,
                                                                                  float(self._widget.valve2TimeEdit.text()))
                                                     )
        # Motor
        self._widget.desiredPositionSlider.valueChanged.connect(lambda: self.com_send_motors(self._widget.desiredPositionSlider.value(),
                                                                                             float(self._widget.maxVelocityEdit.text())
                                                                                             )
                                                                )

        # Parameters
        self.serial_port_name = None
        self.serial_baudrate = None
        self.portIsOpened = False


        # Serial Data Receiving Thread
        self.com_receive_thread = threading.Thread(target=self.com_receive)
        self.com_receive_thread.setDaemon(True)
        self.com_receive_thread.start()

    def com_open_connection(self):
        if self.serial_baudrate == None or self.serial_port_name == None:
            rospy.logerr("Please select serial port name and baudrate")
        else:
            try:
                self.ser = serial.Serial(self.serial_port_name,
                                         baudrate=self.serial_baudrate
                                         )
                rospy.logwarn("Port {} successfully open with baudrate {}".format(self.serial_port_name,
                                                                                  self.serial_baudrate))
                self.portIsOpened = True
            except serial.SerialException:
                rospy.logerr("Cannot open the port {}".format(self.serial_port_name))
                self.portIsOpened = False

    def com_close_connection(self):
        if self.serial_baudrate == None or self.serial_port_name == None:
            rospy.logerr("Please select serial port name and baudrate")
        else:
            if self.portIsOpened:
                self.ser.close()
                rospy.logwarn("Port {} successfully closed".format(self.serial_port_name))
                self.portIsOpened = False
            else:
                rospy.logerr("Cannot close the port {}".format(self.serial_port_name))

    def com_settings(self):
        rospy.loginfo("Settings opened")
        self.dialog = QDialog()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_prototype_app'), 'ui', 'settingsdialog.ui')
        loadUi(ui_file, self.dialog)

        available_buadrates = [115200, 19200, 9600]
        for baudrate in available_buadrates:
            self.dialog.baudRateBox.addItem("{}".format(baudrate), baudrate)

        available_ports = glob.glob('/dev/tracz*') \
                          + glob.glob('/dev/ttyUSB*') \
                          + glob.glob('/dev/ttyACM*') \
                          + glob.glob('/dev/rfcomm*')
        for port in available_ports:
            self.dialog.serialPortInfoListBox.addItem("{}".format(port))

        self.dialog.applyButton.clicked.connect(self.dialog_return)

        result = self.dialog.exec_()
        if result:
            self.serial_baudrate = self.dialog.baudRateBox.currentText()
            self.serial_port_name = self.dialog.serialPortInfoListBox.currentText()
            rospy.logwarn("Selected port name {} and baudrate {}".format(self.serial_port_name,
                                                                         self.serial_baudrate))

    def dialog_return(self):
        self.dialog.accept()

    def com_receive(self):
        msg_recv = sensor_msg()
        while True:
            if self.portIsOpened:
                try:
                    sync_byte = self.ser.read(1)
                    if sync_byte == '\xFF':
                        size_byte = struct.unpack("B", self.ser.read(1))[0]
                        s = self.ser.read(size_byte)
                        msg_recv.ParseFromString(s)
                        rospy.logdebug("Received data: {} {} {} {} {}".format(msg_recv.pressure,
                                                                             msg_recv.motor_current,
                                                                             msg_recv.motor_actual_position,
                                                                             msg_recv.motor_actual_velocity,
                                                                             msg_recv.battery_level)
                                      )
                        self._widget.currentPositionLabel.setText("{:.3f} m".format(msg_recv.motor_actual_position))
                        self._widget.velocityLabel.setText("{:.3f} m/s".format(msg_recv.motor_actual_velocity))
                        self._widget.pressureDifferenceLabel.setText("{:.3f} MPa".format(msg_recv.pressure))
                        self._widget.currentLabel.setText("{:.2f} mA".format(msg_recv.motor_current))
                        self._widget.batteryVoltageLabel.setText("{:.2f} V".format(msg_recv.battery_level))
                except:
                    rospy.logerr("Cannot decode the frame from the microcontroller")
            else:
                time.sleep(1)

    def com_send_motors(self, motor_desired_position, motor_max_velocity):
        if self.portIsOpened:
            rospy.loginfo("Send data motors {} {}".format(motor_desired_position,
                                                          motor_max_velocity
                                                          )
                          )
            msg_send = motor_msg()
            msg_send.desired_position = motor_desired_position
            msg_send.max_velocity = motor_max_velocity
            msg_sync_byte = '\xFF'
            msg_id = '\x01'
            msg_dummy = '\x00\x00\x00\x00'
            frame_send = "{}{}{}{}{}".format(msg_sync_byte,
                                           msg_id,
                                           struct.pack("B", len(msg_send.SerializeToString())),
                                           msg_send.SerializeToString(),
                                           msg_dummy
                                           )
            for i in range(len(frame_send)):
                rospy.logwarn(struct.unpack("B", frame_send[i])[0])
            self.ser.write(frame_send)

    def com_send_valves(self, valve1_on, valve1_reset_time, valve2_on, valve2_reset_time):
        if self.portIsOpened:
            rospy.loginfo("Send data valves {} {} {} {}".format(valve1_on,
                                                                valve1_reset_time,
                                                                valve2_on,
                                                                valve2_reset_time
                                                                )
                          )
            msg_send = valves_msg()
            msg_send.valve1_open = valve1_on
            msg_send.valve1_open_time = valve1_reset_time
            msg_send.valve2_open = valve2_on
            msg_send.valve2_open_time = valve2_reset_time
            msg_sync_byte = '\xFF'
            msg_id = '\x02'
            frame_send = "{}{}{}{}".format(msg_sync_byte,
                                           msg_id,
                                           struct.pack("B", len(msg_send.SerializeToString())),
                                           msg_send.SerializeToString()
                                           )
            for i in range(len(frame_send)):
                rospy.loginfo(struct.unpack("B", frame_send[i])[0])
            self.ser.write(frame_send)


    # def shutdown_plugin(self):
    #     # TODO unregister all publishers here
    #     pass
    #
    # def save_settings(self, plugin_settings, instance_settings):
    #     # TODO save intrinsic configuration, usually using:
    #     # instance_settings.set_value(k, v)
    #     pass
    #
    # def restore_settings(self, plugin_settings, instance_settings):
    #     # TODO restore intrinsic configuration, usually using:
    #     # v = instance_settings.value(k)
    #     pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog