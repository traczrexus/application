#!/usr/bin/env python

import os
import rospy
import rospkg
import serial
import glob
import threading
import time
import struct

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget, QDialog

from sensors_msg_pb2 import sensor_msg
from motor_msg_pb2 import motor_msg
from valves_msg_pb2 import valves_msg

from std_msgs.msg import Float64
from std_msgs.msg import Bool
from sensor_msgs.msg import BatteryState
from std_srvs.srv import SetBool

##### GUI objects cheatsheet #####

### Desired position and velocity
# desiredPositionEdit
# desiredPositionSlider
# maxVelocityEdit

### Sensors data
# currentPositionLabel
# velocityLabel
# pressureDifferenceLabel
# currentLabel
# batteryVoltageLabel

### Valves manipulation
# valve1OnButton
# valve1OffButton
# valve1ResetButton
# valve1TimeEdit
# valve2OnButton
# valve2OffButton
# valve2ResetButton
# valve2TimeEdit

### Stop
# stopButton


class RXSM(Plugin):
    def __init__(self, context):
        super(RXSM, self).__init__(context)
        self.setObjectName('RXSM')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_prototype_app'), 'ui', 'tracz_rxsm.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('TraczRXSM')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        rospy.Subscriber('/rxsm/sods', Bool, self.clb_sods )
        rospy.Subscriber('/rxsm/lo', Bool, self.clb_lo )
        rospy.Subscriber('/rxsm/soe', Bool, self.clb_soe )


    def clb_sods(self, data):
        self._widget.sods_value.setText("{}".format(data.data))

    def clb_lo(self, data):
        self._widget.lo_value.setText("{}".format(data.data))

    def clb_soe(self, data):
        self._widget.soe_value.setText("{}".format(data.data))


class Pneumatics(Plugin):
    def __init__(self, context):
        super(Pneumatics, self).__init__(context)
        self.setObjectName('Pneumatics')
        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_prototype_app'), 'ui', 'tracz_pneumatics.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('TraczPneumatics')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        rospy.wait_for_service('/valve/ambient')
        rospy.wait_for_service('/valve/tank')

        self.valve_ambient = rospy.ServiceProxy('/valve/ambient', SetBool)
        self.valve_tank = rospy.ServiceProxy('/valve/tank', SetBool)


        # Valves
        self._widget.valveAmbientOnButton.clicked.connect(self.com_send_valve_ambient_on  )
        self._widget.valveAmbientOffButton.clicked.connect(self.com_send_valve_ambient_off )
        self._widget.valveTankOnButton.clicked.connect(lambda: self.com_send_valve_tank(1)        )
        self._widget.valveTankOffButton.clicked.connect(lambda: self.com_send_valve_tank(0)       )

        rospy.Subscriber("/pneumatics/differential_pressure", Float64, self.clb_diff_pressure )
        rospy.Subscriber("/pneumatics/valve/ambient/state", Bool, self.clb_ambient )
        rospy.Subscriber("/pneumatics/valve/tank/state", Bool, self.clb_tank )



    def clb_diff_pressure(self, data):
        self._widget.diff_pressure_value.setText("{:.3f} Bar".format(data.data))

    def clb_ambient(self, data):
        if data.data == True:
            self._widget.valve_ambient_value.setText("Open")
        else:
            self._widget.valve_ambient_value.setText("Close")

    def clb_tank(self, data):
        if data.data == True:
            self._widget.valve_tank_value.setText("Open")
        else:
            self._widget.valve_tank_value.setText("Close")

    def com_send_valve_ambient_on(self):
        resp1 = self.valve_ambient(True)

    def com_send_valve_ambient_off(self):
        resp1 = self.valve_ambient(False)

    def com_send_valve_tank(self, valve):
        if valve == 1:
            resp1 = self.valve_tank(True)
        else:
            resp1 = self.valve_tank(False)