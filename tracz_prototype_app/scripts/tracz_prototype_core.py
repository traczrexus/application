#!/usr/bin/env python

import rospy
import serial
import threading
import time
import struct

from std_msgs.msg import Float64
from std_msgs.msg import Bool

from tracz_mainboard_pb2 import tracz_mainboard_msg
from tracz_sensorboard_pb2 import tracz_sensorboard_msg

from std_srvs.srv import SetBool

msg_cnt=0


def com_receive():
    global portIsClosed, ser

    pub_rxsm_sods = rospy.Publisher("/rxsm/sods", Bool, queue_size=10)
    pub_rxsm_lo = rospy.Publisher("/rxsm/lo", Bool, queue_size=10)
    pub_rxsm_soe = rospy.Publisher("/rxsm/soe", Bool, queue_size=10)

    pub_motor_position_actual = rospy.Publisher("/motor/position/actual", Float64, queue_size=10)
    pub_motor_velocity_actual = rospy.Publisher("/motor/velocity/actual", Float64, queue_size=10)
    pub_motor_current = rospy.Publisher("/motor/current", Float64, queue_size=10)

    pub_pneumatics_differential_pressure = rospy.Publisher("/pneumatics/differential_pressure", Float64, queue_size=10)
    pub_pneumatics_ambient_valve = rospy.Publisher("/pneumatics/valve/ambient/state", Bool, queue_size=10)
    pub_pneumatics_tank_valve = rospy.Publisher("/pneumatics/valve/tank/state", Bool, queue_size=10)


    msg_rxsm_sods = Bool()
    msg_rxsm_lo = Bool()
    msg_rxsm_soe = Bool()

    msg_motor_position_actual = Float64()
    msg_motor_velocity_actual = Float64()
    msg_motor_current = Float64()
    msg_pneumatics_differential_pressure = Float64()

    msg_pneumatics_ambient_valve = Bool()
    msg_pneumatics_tank_valve = Bool()

    msg_mainboard_recv = tracz_mainboard_msg()
    msg_sensorboard_recv = tracz_sensorboard_msg()



    # msg_mainboard_recv_tmp = tracz_mainboard_msg.
    # msg_mainboard_recv

    while True:
        if not portIsClosed:
            try:
                sync_byte = ser.read(1)
                # print("sync byte 1 {}".format(struct.unpack("B", sync_byte)[0]))
                if sync_byte == '\xFF':
                    sync_byte = ser.read(1)
                    # print("sync byte 2 {}".format(struct.unpack("B", sync_byte)[0]))
                    if sync_byte == '\xFF':
                        # print("SYNCED")
                        msg_id = ser.read(1)
                        # print("msg id {}".format(struct.unpack("B", msg_id)[0]))
                        size_byte = struct.unpack("B", ser.read(1))[0]
                        # print("size_byte {}".format(size_byte))
                        msg_cnt = ser.read(1)
                        # print("msg_cnt {}".format(struct.unpack("B", msg_cnt)[0]))
                        if msg_id == '\x01':

                            s = ser.read(size_byte)
                            # print(struct.unpack("B", s[0]))

                            # for i in range(size_byte):
                            #     msg_byte = ser.read(1)
                            #     print("{} ".format(struct.unpack("B", s[i])[0]))


                            msg_mainboard_recv.ParseFromString(s)
                            rospy.logdebug("Motorboard received data: {} {} {} {} {}".format(msg_mainboard_recv.SODS,
                                                                                             msg_mainboard_recv.LO,
                                                                                             msg_mainboard_recv.SOE,
                                                                                             msg_mainboard_recv.ambient_valve_state,
                                                                                             msg_mainboard_recv.tank_valve_state)
                                           )

                            msg_rxsm_sods.data = msg_mainboard_recv.SODS
                            msg_rxsm_lo.data = msg_mainboard_recv.LO
                            msg_rxsm_soe.data = msg_mainboard_recv.SOE
                            #
                            # msg_motor_position_actual.data = msg_mainboard_recv.motor_actual_position
                            # msg_motor_velocity_actual.data = msg_mainboard_recv.motor_actual_velocity
                            # msg_motor_current.data = msg_mainboard_recv.motor_current
                            #
                            msg_pneumatics_differential_pressure.data = msg_mainboard_recv.differential_pressure
                            msg_pneumatics_ambient_valve.data = msg_mainboard_recv.ambient_valve_state
                            msg_pneumatics_tank_valve.data = msg_mainboard_recv.tank_valve_state
                            #
                            pub_rxsm_sods.publish(msg_rxsm_sods)
                            pub_rxsm_lo.publish(msg_rxsm_lo)
                            pub_rxsm_soe.publish(msg_rxsm_soe)
                            #
                            # pub_motor_position_actual.publish(msg_motor_position_actual)
                            # pub_motor_velocity_actual.publish(msg_motor_velocity_actual)
                            # pub_motor_current.publish(msg_motor_current)
                            #
                            pub_pneumatics_differential_pressure.publish(msg_pneumatics_differential_pressure)
                            pub_pneumatics_ambient_valve.publish(msg_pneumatics_ambient_valve)
                            pub_pneumatics_tank_valve.publish(msg_pneumatics_tank_valve)

                        elif msg_id == '\x02':
                            s = ser.read(size_byte)
                            msg_sensorboard_recv.ParseFromString(s)
                            # rospy.loginfo("Sensorboard received data: {0:.2f}".format(msg_sensorboard_recv.angular_velocity_z))

                        msg_crc = ser.read(1)
                        # print("crc1 {}".format(struct.unpack("B", msg_crc)[0]))
                        msg_crc = ser.read(1)
                        # print("crc2 {}".format(struct.unpack("B", msg_crc)[0]))

            except:
                rospy.logerr("Cannot decode the frame from the microcontroller")
        else:
            time.sleep(1)


def handle_ambient_valve(req):
    if req.data == True:
        send_frame('\x53')
    else:
        send_frame('\x54')

    return [req.data, ""]


def handle_tank_valve(req):
    if req.data == True:
        send_frame('\x51')
    else:
        send_frame('\x52')

    return [req.data, ""]

def send_frame(msg_id):
    global msg_cnt
    msg_cnt+=1
    frame_send = "{}{}{}{}".format('\xFF',
                                   '\xFF',
                                   struct.pack("B", msg_cnt),
                                   msg_id
                                   )
    for i in range(len(frame_send)):
        rospy.logdebug(struct.unpack("B", frame_send[i])[0])
    ser.write(frame_send)

def node():
    global portIsClosed, ser
    timeout_ctrl = 0.0

    rospy.init_node('tracz_core')

    port_name = rospy.get_param('~port','/dev/tracz')
    portIsClosed = True

    while portIsClosed:
        try:
            ser = serial.Serial(port_name, baudrate='38400')
            rospy.logwarn("Port {} successfully open".format(port_name))
            portIsClosed = False
        except serial.SerialException:
            rospy.logerr("Cannot open a port {}".format(port_name))
            time.sleep(1)
            portIsClosed = True

    com_receive_thread = threading.Thread(target=com_receive)
    com_receive_thread.setDaemon(True)
    com_receive_thread.start()

    rospy.Service('/valve/ambient', SetBool, handle_ambient_valve)
    rospy.Service('/valve/tank', SetBool, handle_tank_valve)

    rate_in_Hz = 0.1
    # timeout_ctrl_step = 1/rate_in_Hz
    rate = rospy.Rate(rate_in_Hz)
    while not rospy.is_shutdown():
    #     com_send_valves(1, 1, 1, 1)
    #
        rate.sleep()

if __name__ == '__main__':
    import signal
    for sig in ('TERM', 'HUP', 'INT'):
        signal.signal(getattr(signal, 'SIG'+sig), quit)

    try:
        node()
    except rospy.ROSInterruptException:
        pass