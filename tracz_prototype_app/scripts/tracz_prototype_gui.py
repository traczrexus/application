#!/usr/bin/env python

import os
import rospy
import rospkg
import serial
import glob
import threading
import time
import struct

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget, QDialog

from sensors_msg_pb2 import sensor_msg
from motor_msg_pb2 import motor_msg
from valves_msg_pb2 import valves_msg

from std_msgs.msg import Float64
from sensor_msgs.msg import BatteryState

##### GUI objects cheatsheet #####

### Desired position and velocity
# desiredPositionEdit
# desiredPositionSlider
# maxVelocityEdit

### Sensors data
# currentPositionLabel
# velocityLabel
# pressureDifferenceLabel
# currentLabel
# batteryVoltageLabel

### Valves manipulation
# valve1OnButton
# valve1OffButton
# valve1ResetButton
# valve1TimeEdit
# valve2OnButton
# valve2OffButton
# valve2ResetButton
# valve2TimeEdit

### Stop
# stopButton


class TraczPlugin(Plugin):

    def __init__(self, context):
        super(TraczPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('TraczPlugin')

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('tracz_prototype_app'), 'ui', 'tracz_app.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('TraczPluginUi')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

        ## signals and slots connection
        # Communication port configuration
        # self._widget.comOpenButton.clicked.connect(self.com_open_connection)
        # self._widget.comCloseButton.clicked.connect(self.com_close_connection)
        # self._widget.comSettingsButton.clicked.connect(self.com_settings)

        # Valves
        # self._widget.valve1OnButton.clicked.connect(lambda: self.com_send_valves(1,
        #                                                                          float(self._widget.valve1TimeEdit.text()),
        #                                                                          0,
        #                                                                          float(self._widget.valve2TimeEdit.text()))
        #                                             )
        # self._widget.valve1OffButton.clicked.connect(lambda: self.com_send_valves(0,
        #                                                                           float(self._widget.valve1TimeEdit.text()),
        #                                                                           0,
        #                                                                           float(self._widget.valve2TimeEdit.text()))
        #                                              )
        # self._widget.valve2OnButton.clicked.connect(lambda: self.com_send_valves(0,
        #                                                                          float(self._widget.valve1TimeEdit.text()),
        #                                                                          1,
        #                                                                          float(self._widget.valve2TimeEdit.text()))
        #                                             )
        # self._widget.valve2OffButton.clicked.connect(lambda: self.com_send_valves(0,
        #                                                                           float(self._widget.valve1TimeEdit.text()),
        #                                                                           0,
        #                                                                           float(self._widget.valve2TimeEdit.text()))
        #                                              )
        # # Motor
        # self._widget.desiredPositionSlider.valueChanged.connect(lambda: self.com_send_motors(self._widget.desiredPositionSlider.value(),
        #                                                                                      float(self._widget.maxVelocityEdit.text())
        #                                                                                      )
        #                                                         )


        # rospy.Subscriber('/motor/position/actual', Float64, self.clb_motor_position )
        # rospy.Subscriber('/motor/velocity/actual', Float64, self.clb_motor_velocity )
        # rospy.Subscriber('/motor/current', Float64, self.clb_motor_current )
        # rospy.Subscriber('/pneumatics/differential_pressure', Float64, self.clb_pressure )
        # rospy.Subscriber('/battery', BatteryState, self.clb_baterry )



    def clb_pressure(self, data):
        self._widget.pressureDifferenceLabel.setText("{:.3f} MPa".format(data.data))

    def clb_motor_position(self, data):
        self._widget.currentPositionLabel.setText("{:.3f} m".format(data.data))

    def clb_motor_velocity(self, data):
        self._widget.velocityLabel.setText("{:.3f} m/s".format(data.data))

    def clb_motor_current(self, data):
        self._widget.currentLabel.setText("{:.3f} mA".format(data.data))

    def clb_baterry(self, data):
        self._widget.batteryVoltageLabel.setText("{:.3f} V".format(data.voltage))